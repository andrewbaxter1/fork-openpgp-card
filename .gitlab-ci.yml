# SPDX-FileCopyrightText: 2021-2023 Heiko Schaefer <heiko@schaefer.name>
# SPDX-FileCopyrightText: 2021-2022 Nora Widdecke <mail@nora.pink>
# SPDX-License-Identifier: CC0-1.0

stages:
  - lint
  - test
  - virtual-cards
  - udeps
  - hw-builddeps
  - hw-import
  - hw-keygen

variables:
  CARGO_HOME: cargo/
  CARGO_TERM_VERBOSE: 'true'

cache: &general_cache_config
  # default key is default
  # default policy is pull-push
  paths:
    - target/
    - $CARGO_HOME

.report-rust: &report-rust
  - cargo --version
  - rustc --version

reuse:
  stage: lint
  image:
    name: fsfe/reuse:latest
    entrypoint: [ "" ]
  script:
    - reuse lint
  dependencies: [ ]
  cache: [ ]

cargo-fmt:
  stage: lint
  image: rust:latest
  before_script:
    - export CARGO_HOME="/usr/local/cargo" # HACK: set back to its default
    - rustup toolchain install nightly && rustup component add --toolchain nightly rustfmt
    - *report-rust
  script:
    - cargo +nightly fmt -- --check
  cache: [ ]

cargo-clippy:
  stage: lint
  image: rust:latest
  before_script:
    - mkdir -p /run/user/$UID
    - apt update -y -qq
    - apt install -y -qq --no-install-recommends git clang make pkg-config nettle-dev libssl-dev capnproto ca-certificates libpcsclite-dev
    - apt clean
    - *report-rust
  script:
    - rustup component add clippy
    - cargo clippy --verbose --tests -- -D warnings
  allow_failure: true
  cache:
    # inherit all general cache settings
    <<: *general_cache_config
    # override the key
    key: "rust-latest"

semver-checks:
  stage: lint
  image: rust:latest
  before_script:
    - mkdir -p /run/user/$UID
    - apt update -y -qq
    - apt install -y -qq --no-install-recommends git clang make cmake pkg-config nettle-dev libssl-dev capnproto ca-certificates libpcsclite-dev
    - apt clean
    - *report-rust
  script:
    - cargo install cargo-semver-checks
    - cargo semver-checks -p card-backend -p card-backend-pcsc -p card-backend-scdc -p openpgp-card -p openpgp-card-sequoia
  allow_failure: true
  cache:
    # inherit all general cache settings
    <<: *general_cache_config
    # override the key
    key: "rust-latest"

cargo-deny:
  stage: lint
  image: rust:latest
  before_script:
    # The cargo deny binary is cached, so installing it errors if it was
    # restored from cache. Ignore that error.
    # Clear the cache to force a rebuild.
    - cargo install --locked cargo-deny || true
    - *report-rust
  script:
    - cargo deny check
  cache:
    paths:
      - cargo/bin/cargo-deny
    key: "deny"

udeps:
  stage: udeps
  needs: [ ]
  image: rustlang/rust:nightly-slim
  before_script:
    - mkdir -p /run/user/$UID
    - apt update -y -qq
    - apt install -y -qq --no-install-recommends curl git clang make pkg-config nettle-dev libssl-dev capnproto ca-certificates libpcsclite-dev
    - apt clean
    - cargo install cargo-udeps
  script:
    - cargo udeps --workspace --all-features --all-targets
  cache: [ ]

cargo-test:
  stage: test
  image: rust:latest
  before_script:
    - mkdir -p /run/user/$UID
    - apt update -y -qq
    - apt install -y -qq --no-install-recommends git clang make pkg-config nettle-dev libssl-dev capnproto ca-certificates libpcsclite-dev
    - apt clean
    - *report-rust
  script:
    # there is no virtual card in this image, so subplot does not generate tests
    # that would require one.
    - cargo test
  cache:
    # inherit all general cache settings
    <<: *general_cache_config
    # override the key
    key: "rust-latest"

cargo-test-debian-bookworm:
  stage: test
  image: debian:bookworm-slim
  before_script:
    - mkdir -p /run/user/$UID
    - apt update -y -qq
    - apt install -y -qq --no-install-recommends git rustc cargo clang make pkg-config nettle-dev libssl-dev capnproto ca-certificates libpcsclite-dev
    - apt clean
    - *report-rust
    - cargo update
    - cargo update -p sequoia-openpgp --precise 1.15.0
    - cargo update -p buffered-reader --precise 1.1.5
    - cargo update -p regex --precise 1.9.6
    - cargo update -p lalrpop@0.20.0 --precise 0.19.12 # hack to work with Rust 1.63
    - cargo update -p petgraph --precise 0.6.3 # hack to work with Rust 1.63
  script:
    # there is no virtual card in this image, so subplot does not generate tests
    # that would require one.
    - cargo test
  allow_failure: true
  cache:
    # inherit all general cache settings
    <<: *general_cache_config
    # override the key
    key: "bookworm"

run_cardtest_smartpgp:
  stage: virtual-cards
  image: registry.gitlab.com/openpgp-card/virtual-cards/smartpgp-builddeps
  before_script:
    - export PATH="$HOME/.cargo/bin:$PATH"
    - *report-rust
  script:
    - sh /start.sh
#    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tools --bin opgpcard -- status
#    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tools --bin opgpcard -- info
    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tests --bin import -- $CONFIG
    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tests --bin keygen -- $CONFIG
  variables:
    CONFIG: "card-functionality/ci/virt-smartpgp.toml"
  cache:
    # inherit all general cache settings
    <<: *general_cache_config
    # override the key
    # (the base image of run_cardtest uses bookworm)
    key: "bookworm"

run_cardtest_opcard_rs:
  stage: virtual-cards
  image: registry.gitlab.com/openpgp-card/virtual-cards/opcard-rs-tools
  before_script:
    - export PATH="$HOME/.cargo/bin:$PATH"
    - *report-rust
  script:
    - sh /start.sh
#    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tools --bin opgpcard -- status
#    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tools --bin opgpcard -- info
    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tests --bin import -- $CONFIG
    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tests --bin keygen -- $CONFIG
  variables:
    CONFIG: "card-functionality/ci/virt-opcard-rs.toml"
  cache:
    # inherit all general cache settings
    <<: *general_cache_config
    # override the key
    # (the base image is separate from the other virtual cards)
    key: "opcard_rs"

run_cardtest_canokey:
  stage: virtual-cards
  image: registry.gitlab.com/openpgp-card/virtual-cards/canokey-builddeps
  before_script:
    - export PATH="$HOME/.cargo/bin:$PATH"
    - *report-rust
  script:
    - sh /start.sh && sleep 10
    #    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tools --bin opgpcard -- status
    #    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tools --bin opgpcard -- info
    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tests --bin import -- $CONFIG
    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tests --bin keygen -- $CONFIG
  variables:
    CONFIG: "card-functionality/ci/virt-canokey.toml"
  cache:
    # inherit all general cache settings
    <<: *general_cache_config
    # override the key
    # (the base image of canokey-builddeps is distinct from the other virtual card images)
    key: "canokey"

run_cardtest_ykneo:
  stage: virtual-cards
  image: registry.gitlab.com/openpgp-card/virtual-cards/ykneo-builddeps
  before_script:
    - export PATH="$HOME/.cargo/bin:$PATH"
    - *report-rust
    - cargo update
    - cargo update -p sequoia-openpgp --precise 1.15.0
    - cargo update -p buffered-reader --precise 1.1.5
    - cargo update -p regex --precise 1.9.6
    - cargo update -p lalrpop@0.20.0 --precise 0.19.12 # hack to work with Rust 1.63
    - cargo update -p petgraph --precise 0.6.3 # hack to work with Rust 1.63
  script:
    - sh /start.sh
#    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tools --bin opgpcard -- status
#    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tools --bin opgpcard -- info
    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tests --bin import -- $CONFIG
    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tests --bin keygen -- $CONFIG
  variables:
    CONFIG: "card-functionality/ci/virt-ykneo.toml"
  cache:
    # inherit all general cache settings
    <<: *general_cache_config
    # override the key
    # (the base image of run_cardtest uses bookworm)
    key: "bookworm"

run_cardtest_fluffypgp:
  stage: virtual-cards
  image: registry.gitlab.com/openpgp-card/virtual-cards/fluffypgp-builddeps
  before_script:
    - export PATH="$HOME/.cargo/bin:$PATH"
    - *report-rust
    - cargo update
    - cargo update -p sequoia-openpgp --precise 1.15.0
    - cargo update -p buffered-reader --precise 1.1.5
    - cargo update -p regex --precise 1.9.6
    - cargo update -p lalrpop@0.20.0 --precise 0.19.12 # hack to work with Rust 1.63
    - cargo update -p petgraph --precise 0.6.3 # hack to work with Rust 1.63
  script:
    - sh /start.sh
#    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tools --bin opgpcard -- status
#    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tools --bin opgpcard -- info
    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tests --bin import -- $CONFIG
    - RUST_BACKTRACE=1 cargo run -p openpgp-card-tests --bin keygen -- $CONFIG
  variables:
    CONFIG: "card-functionality/ci/virt-fluffypgp.toml"
  cache:
    # inherit all general cache settings
    <<: *general_cache_config
    # override the key
    # (the base image of run_cardtest uses bookworm)
    key: "bookworm"

# disabled for now
.hardware-builddeps:
  stage: hw-builddeps
  needs: [ ]
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker pull $IMAGE_LATEST || true
    - docker build --cache-from $IMAGE_LATEST --file=$DOCKERFILE -t $IMAGE_LATEST -t $IMAGE_SHA .
    - docker push $IMAGE_SHA
    - docker push $IMAGE_LATEST
  only:
    variables:
      # Forks of this project do not have access to cookiejar's hardware tokens
      - $CI_PROJECT_NAMESPACE == "openpgp-card"
    changes:
      - card-functionality/docker/Dockerfile.hardware-builddeps
      - .gitlab-ci.yml
  variables:
    IMAGE_LATEST: $CI_REGISTRY_IMAGE/hardware-builddeps:latest
    IMAGE_SHA: $CI_REGISTRY_IMAGE/hardware-builddeps:$CI_COMMIT_SHA
    DOCKERFILE: card-functionality/docker/Dockerfile.hardware-builddeps
  cache: [ ]

.hw-test-template:
  image: registry.gitlab.com/openpgp-card/openpgp-card/hardware-builddeps
  only:
    variables:
      # Forks of this project do not have access to cookiejar's hardware tokens
      - $CI_PROJECT_NAMESPACE == "openpgp-card"
  tags:
    - card
  parallel:
    matrix:
      - CONFIG:
          - "card-functionality/ci/basic-2_1.toml"
          - "card-functionality/ci/nitro-pro1.toml"
          - "card-functionality/ci/nitro-pro2.toml"
          - "card-functionality/ci/nitro-start.toml"
  before_script:
    - *report-rust
  script:
    - RUST_BACKTRACE=1 RUST_LOG=debug cargo run --bin $ARG -- $CONFIG
  cache:
    # inherit all general cache settings
    <<: *general_cache_config
    # cookiejar does not access the cache of the shared gitlab runners,
    # so use a different key for clarity
    key: "cookiejar"

# disabled for now
.import:
  extends: .hw-test-template
  stage: hw-import
  variables:
    ARG: import

# disabled for now
.keygen:
  extends: .hw-test-template
  stage: hw-keygen
  variables:
    ARG: keygen
